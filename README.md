# Lab 2
### Repository Structure
    lab2 ($PROJ_DIR)
      |
      |--> adamacc/src/main/scala
              |
              |--> reducer (accelerator cores used for reducer)
              |--> sha3 (your implementation)
              |--> common (readers and writers for accelerator cores)
              | abstract core and abstract template files
      |        
      |--> aws-fpga-adamacc (submodule; a forked aws-fpga remote)
              |--> hdk/cl/developer_designs/adamacc (our code; this directory is also what $CL_DIR is set to)
                         |--> software/runtime (C/C++ control programs for our applications/benchmarks)
                                     |--> include/rocc.h (RoCC instruction encodings)
                                     |--> include/testhelp.h (C wrappers on top of aws-fpga-mgmt libraries)
                         |--> verif/scripts (VCS simulation related infrastructure files)                    
              | other AWS files 
      | 
      /src/main/scala/adamaccaws
              |--> Configs.scala (top level system configs to build)
              | other top level system files
              
### How to Prepare/Ready Your Repo 
* Do this on all your CS machine, C5 (c5.9xlarge) instance, and F1 (f1.2xlarge) instance
* For CS machine, create a dir for lab2
    ```
    > mkdir <your-lab2>
    > cd <your-lab2>
    ```
* For C5 and F1 instances, go to the data dir. Do NOT use your home dir
    ```
    > cd ~/src/project_data/
    ```
* For C5 and F1 instances, install Java8 and SBT
    ```
    > curl https://bintray.com/sbt/rpm/rpm | sudo tee /etc/yum.repos.d/bintray-sbt-rpm.repo
    > sudo yum install sbt
    > sudo yum install java-1.8.0-openjdk-devel
    ```
* For your first run, clone the repo and setup submodules, run "source
javasetup.sh" when you are on cs machine. 
    ```
    > git clone https://gitlab.oit.duke.edu/jz226/cs590-sp2021-lab2.git
    > cd cs590-sp2021-lab2
    > source javasetup.sh (only on CS machine)
    > bash init.sh
    ```
* After that, login with BASH (not CSH) and setup environment ($CL_DIR and
VCS/Xilinx licences). Run the following command whenever you login to your
machine. 
    ```
    > bash
    > source javasetup.sh (only on CS machine)
    > source setup.sh
    ```
* You should check if your are using java8, refer to lab1

* You should check to see that your env variable $CL_DIR is set up correctly
    ```
    > echo $CL_DIR
    ```
* You should check to see that your env variable $PROJ_DIR is set up correctly (points to the top level of the lab2 repo)
    ```
    > echo $PROJ_DIR
    ```

### How to Build An Example System and Generate Verilog
* Do this on your CS machine
* Go to the top level of the lab2 repo
* Pick a configuration from cs590-sp2021-lab2/src/main/scala/adamaccaws/Configs.scala to build
* We will use "ReducerTestConfig" in this example
    ```
    > cd $PROJ_DIR/vsim
    > make verilog CONFIG=ReducerTestConfig
    ```
* Once the build succeeds, the Verilog output is placed in $PROJ_DIR/vsim/generated-src/adamaccaws.TestHarness.ReducerTestConfig.v    

### How to Set Up a VCS Simulation of the Built Example System (You need
only ONE terminal window twith two tmux windows to run simulation)
* Do this on your CS machine
* Preparations
    * In order to use VCS Simulation on the CS machines, we need to checkout the "simulation" branch of the aws-fpga-adamacc 
        ```
        > cd $PROJ_DIR/aws-fpga-adamacc
        > git checkout simulation
        ```
    * Move the generated Verilog to our design directory under $CL_DIR and add necessary simulation headers by executing the shell script cs590-sp2021-lab2/vsim/move-to-sim.sh
    * Before running the script, open it and make sure the env var CONFIG is set to ReducerTestConfig (or whatever config you are compiling)
        ```
        > cd $PROJ_DIR/vsim
        > ./move-to-sim.sh
        ```
    * Read over aws-fpga documentation https://github.com/aws/aws-fpga for details
* Starting hardware simulator
    * Open one tmux window; let's call this window W1. For how to use
    tmux, refer to https://tmuxcheatsheet.com/
    * Make sure all the necessary VCS and Xilinx licences are in place
    * Make sure you source all the javasetup.sh and setup.sh. 
    * Make sure $CL_DIR is set to point at $PROJ_DIR/aws-fpga-adamacc/hdk/cl/developer_designs/adamacc
    * Run these commands on the command line of W1
        ```
        > cd $PROJ_DIR/aws-fpga-adamacc
        > source hdk_setup.sh
        ```
    * Compile and run the VCS simulation with the generated Verilog
        ``` 
        > cd $CL_DIR/verif/scripts
        > make
        ```
* Starting software application/benchmark
    * Open another tmux window; let's call this window W2
    * Also, make sure you sure javasetup.sh and setup.sh
    * Run these commands on the command line of W2
        ```
        > cd $PROJ_DIR/aws-fpga-adamacc
        > source sdk_setup.sh 
        ```
    * Compile the application/benchmark control program
    * Pick an application/a test from $CL_DIR/software/runtime to build
    * We will use "reducer" in this example. Remember to set "FPGA=0" for when using simulation
        ```
        > cd $CL_DIR/software/runtime
        > make FPGA=0
        > ./bin/reducer
        ```
    * If the test/benchmark runs to completion and the simulator exits, you have succeeded!! (these tests run for a long time in VCS if the input files are large)
* Recording and visualizing waveform
    * In W1, use the provided script to turnon waveform, and start simulation
        ``` 
        > cd $CL_DIR/verif/scripts
        > ./turnon-waves.sh
        > make
        ```
    * In W2, run the application again. Choose a small input size since the simulation with waveform is very slow
    * View the waveform with DVE in open another terminal window using ssh
    -Y, do not type the srun command (like in lab1). Then source the javasetup.sh and setup.sh.
    
        ``` 
        > cd $CL_DIR/verif/scripts
        > ./view_waves.sh ../sim/test_adamacc/test_null.vpd
        ```
    * Turnoff waveform with "turnoff-waves.sh" for faster simulation


### How to Build a Tarball (FPGA image) of the Built Example System on F1
* Do this on your C5 instance, after the generated Verilog is copied from your CS machine
* In order to build a tarball using the latest aws shell, make sure the aws-fpga-adamacc is on master
    ```
    > cd cs590-sp2021-lab2/aws-fpga-adamacc
    > git checkout master
    ```
* Copy the generated Verilog from CS machine to C5 instance or check out the repo on C5 and compile the Verilog
* You can directly copy the file through `scp` if you have the SSH configured. Otherwise download the Verilog file to your local machine, and then pass it to C5
    ```
    > # Run this on CS machine:
    > scp <your-lab2>/cs590-sp2021-lab2/vsim/generated-src/<your-circuit>.v \\
          <your-c5-hostname>:/home/centos/src/project_data/cs590-sp2021-lab2/vsim/generated-src/<your-circuit>.v
    > # Or run this on your local machine:
    > scp <your-cs-machine>:/usr/xtmp/<your-netid>/cs590-sp2021-lab2/vsim/generated-src/<your-circuit>.v ./temp.v
    > scp ./temp.v <your-c5-hostname>:/home/centos/src/project_data/cs590-sp2021-lab2/vsim/generated-src/<your-circuit>.v
    ```
* Move the Verilog file to our design directory under aws-fpga-adamacc
* Comment out the first few lines in init.sh as you do not need to set Synopsys VCS or Xilinx Vivado licenses on EC2 instances
* Make sure you remember to source your setup.sh to set your CL_DIR and your PROJ_DIR
* Before running the script, open it and make sure the env var CONFIG is set to ReducerTestConfig
    ```
    > cd $PROJ_DIR/vsim
    > ./move-to-build.sh
    ```
* Fix number of ID bits for AXI4 connections
* Open up the generated_aws.sv in the design directory and check to see how many ID bits are generated from this design
    ```
    > cd $CL_DIR/design
    ```
* Open the file adamacc_generated.sv and search for **module AdamaccAWSTop**
    ```
    > vim adamacc_generated.sv
    ```
* Search for aw_bits_id to see how many ID bits are generated
* In this example, 6 ID bits are generated
    ```
    <* in adamacc_generated.sv *>
    output [5:0]   axi4_mem_0_aw_bits_id,
    ```
* Change the aws shell to match the number of ID bits
* Open the file adamacc_aws.sv
* Search for io_mem to change the ID bits for the AXI4 interface
    ```
    <* in adamacc_aws.sv *>
    wire [9:0]    io_mem_0_aw_bits_id;
    wire [9:0]    io_mem_0_b_bits_id;
    wire [9:0]    io_mem_0_ar_bits_id;
    wire [9:0]    io_mem_0_r_bits_id;
    ```
* Change them to
    ```
    <* in adamacc_aws.sv *>
    wire [5:0]    io_mem_0_aw_bits_id;
    wire [5:0]    io_mem_0_b_bits_id;
    wire [5:0]    io_mem_0_ar_bits_id;
    wire [5:0]    io_mem_0_r_bits_id;
    ```
* Now we are ready to build the tarball using hte script provided by aws-fpga
    ```
    > cd $CL_DIR/build/scripts
    > ./aws_build_dcp_from_cl.sh -notify 
    if the EMAIL env var is set up, -notify will enable aws to send email when the build is complete
    ```
* 3-4 hours later...
* Check if you have a *.Developer_CL.tar file under
$CL_DIR/build/checkpoints/to_aws/, if so, it is correct.
* The built tarball is ready to be created into an AFI. The AFI can now be loaded onto the FPGA on the F1 instance.

### How to create AFI image and setup FPGA
* AFI creation
    * On C5 instance, upload the tarball to S3 bucket, ask Dr. Wills for
    help if you have permission problem.
        ```
        > aws s3 cp $CL_DIR/build/checkpoints/to_aws/*.Developer_CL.tar s3://cs590-tarballs/tarballs/ 
        NOTE: The trailing '/' is required after tarballs
        ```
    * Start AFI creation. Choose an unique afi-name and an afi-description as you like
        ```
        > aws ec2 create-fpga-image \
            --region us-east-1 \
            --name <make up your own afi-name (should be unique everytime)> \
            --description <make up your own afi-description> \
            --input-storage-location Bucket=cs590-tarballs,Key=tarballs/<X.Developer_CL.tar tar-file name> \
            --logs-storage-location Bucket=cs590-tarballs,Key=logs 
        ```
    * Remember the AFI ID and Global AFI ID returned by the last command (save them in a file). You can check AFI build status using the AFI ID 
        ```
        > aws ec2 describe-fpga-images --fpga-image-ids afi-(some_id_numbers)`
        ```
    * Wait until the AFI state is set to `available` (10~30 minutes)
        ```
        {
            "FpgaImages": [
                {
                ...
                    "State": {
                        "Code": "available"
                    },
                    ...
                    "FpgaImageId": "afi-xxxxxxxxxxxxxxxxx",
                    ...
                }
            ]
        }
        ```
    * You can stop your C5 instance now
* F1 instance setup
    * Do the following steps on your F1 instance
    * Install XDMA driver if you use the F1 instance for the first time (https://github.com/aws/aws-fpga/blob/master/sdk/linux_kernel_drivers/xdma/xdma_install.md)
    * Remove XOCL driver
        ```
        > sudo rmmod xocl
        ```
    * Prepare GCC and Linux Kernel source code
        ```
        > sudo yum groupinstall "Development tools"
        > sudo yum install kernel kernel-devel
        ```
    * Compile and install kernel module
        ```
        > cd cs590-sp2021-lab2/aws-fpga-adamacc/sdk/linux_kernel_drivers/xdma
        > make
        > sudo make install
        ```
* FPGA setup
    * Setup aws fpga SDK in your first run. Make sure your are using the master branch
        ```
        > cd cs590-sp2021-lab2/aws-fpga-adamacc
        > git checkout master
        > source sdk_setup.h
        ```
    * After the XDMA driver is ready, do the following steps to load AFI to FPGA
    * Check the current status of FPGA  
  `sudo fpga-describe-local-image -S 0 -H`

    * Clear any previously-loaded AFI  
  `sudo fpga-clear-local-image  -S 0`

    * Load AFI to FPGA (note that you need to use AGFI(global) ID, NOT AFI
      ID)
      ```
      `sudo fpga-load-local-image -S 0 -I agfi-(some_id_numbers)` 
      ```
    * Verify that the AFI was loaded properly  
  `sudo fpga-describe-local-image -S 0 -R -H`
* Run host programs
    * Setup aws fpga SDK. Make sure your are using the master branch
        ```
        > cd $PROJ_DIR/aws-fpga-adamacc
        > git checkout master
        > source sdk_setup.sh
        ```
    * Build host program with FPGA=1
        ```
        > cd $CL_DIR/software/runtime
        > make FPGA=1
        ```
    * Run host program in sudo
    `sudo ./bin/reducer`
    * You should see outputs similar to this for the reducer example. The printouts are specific to the reducer.cc host program. 
      You can look at reducer.cc to see what these printouts mean. The time printed are in nanoseconds (1 ns = 10^-9 s).
        ```
        ...
        Setting up reduce unit...
        Setting up reduce unit...
        Setting up reduce unit...
        Setting up reduce unit...
        Setting up reduce unit...
        Setting up reduce unit...
        ID : 0 ERROR : 272
        Reducer took: Total Data Transfer Time: 138211
        Rocc_sending_time time: 717120
        Get response_time time: 19742
        Total Run Time: 977085
        Receiving chunk: 640 bytes from 10003400 fpga base
        Successfully receieved 640 bytes from fpga
        12880128801288012880128801288012880128801288012880128801288012880128801288012880
        12880128801288012880128801288012880128801288012880128801288012880128801288012880
        12880128801288012880128801288012880128801288012880128801288012880128801288012880
        12880128801288012880128801288012880128801288012880128801288012880128801288012880
        12880128801288012880128801288012880128801288012880128801288012880128801288012880
        ```
    * Sometimes the FPGA stops working after the host program finishes (actually most of the time). 
      Clear the AFI and reload it to start a clean FPGA.
