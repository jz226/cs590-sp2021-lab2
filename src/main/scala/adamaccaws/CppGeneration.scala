/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamaccaws
package widgets

trait CPPLiteral {
  def typeString: String
  def toC: String
}

trait IntLikeLiteral extends CPPLiteral {
  def bitWidth: Int
  def literalSuffix: String
  def value: BigInt
  def toC = value.toString + literalSuffix

  require(bitWidth >= value.bitLength)
}

case class UInt32(value: BigInt) extends IntLikeLiteral {
  def typeString = "unsigned int"
  def bitWidth = 32
  def literalSuffix = ""
}

case class UInt64(value: BigInt) extends IntLikeLiteral {
  def typeString = "uint64_t"
  def bitWidth = 64
  def literalSuffix = "L"
}

case class CStrLit(val value: String) extends CPPLiteral {
  def typeString = "const char* const"
  def toC = "\"%s\"".format(value)
}

object CppGenerationUtils {
  val indent = "  "

  def genEnum(name: String, values: Seq[String]): String =
    if (values.isEmpty) "" else s"enum $name {%s};\n".format(values mkString ",")

  def genArray[T <: CPPLiteral](name: String, values: Seq[T]): String = {
    val tpe = if (values.nonEmpty) values.head.typeString else "const void* const"
    val prefix = s"static $tpe $name [${math.max(values.size, 1)}] = {\n"
    val body = values map (indent + _.toC) mkString ",\n"
    val suffix = "\n};\n"
    prefix + body + suffix
  }

  def genStatic[T <: CPPLiteral](name: String, value: T): String =
    "static %s %s = %s;\n".format(value.typeString, name, value.toC)

  def genConstStatic[T <: CPPLiteral](name: String, value: T): String =
    "const static %s %s = %s;\n".format(value.typeString, name, value.toC)

  def genConst[T <: CPPLiteral](name: String, value: T): String =
    "const %s %s = %s;\n".format(value.typeString, name, value.toC)

  def genMacro(name: String, value: String = ""): String = s"#define $name $value\n"

  def genMacro[T <: CPPLiteral](name: String, value: T): String =
    "#define %s %s\n".format(name, value.toC)

  def genComment(str: String): String = "// %s\n".format(str)

}


