/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamaccaws

import chisel3._
import adamacc.{reducer, _}
import adamacc.reducer._
// import adamacc.sha3._
import chisel3.core.FixedPoint
import freechips.rocketchip.config.{Config, Field, Parameters}
import freechips.rocketchip.coreplex.{WithNBigCores, WithNMemoryChannels, WithRV32, WithRoccExample}
import freechips.rocketchip.diplomacy.{LazyModule, MonitorsEnabled, ValName}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.tile._
import freechips.rocketchip.coreplex._
import adamacc.common._

object ConfigValName {
  implicit val valName = ValName("TestHarness")
}

import ConfigValName._

class WithAWSMem extends Config((site, here, up) => {
  case CacheBlockBytes => 128
  case ExtMem => MasterPortParams(
    base = 0,
    size = 0x400000000L,
    beatBytes = 64,
    idBits = 12
  )

  //------------------------------------------------------
  // need dummy parameters to trick rocketchip.
  // none of these are needed but they are
  // uninitialized by default and will cause
  // a compile error. ignore these
  case MonitorsEnabled => false
  case TileKey => RocketTileParams()
  case RocketCrossingKey => List(RocketCrossingParams(
    crossingType = SynchronousCrossing(),
    master = TileMasterPortParams(cork = Some(true))
  ))
  case ForceFanoutKey => ForceFanoutParams(false, false, false, false, false)
  //------------------------------------------------------

  // rocc data length
  case XLen => 64
  case NMemChan => 1
})

class WithReducer(nCores: Int) extends Config((site, here, up) => {
  case ModularSystemsKey => up(ModularSystemsKey, site) ++ Seq(ColumnReducerConfig(
    coreParams = ColumnReducerCoreParams(
      nMemXacts = 1, // nMemXacts
      readElementSizes = Seq(32), // read sizes in bytes
      writeElementSizes = Seq(8), // write sizes in bytes
      readLocations = Some(Seq("Mem")),
      writeLocations = Some(Seq("Mem")),
      inputBytes = 32,
      outputBytes = 4
    ),
    nCores = nCores,
    opcode = 2
  ))
})

// class WithSha3(nCores: Int) extends Config((site, here, up) => {})

class ReducerTestConfig extends Config(
  new WithReducer(16) ++
    new WithAWSMem ++
    new WithAdamAcc ++
    new BaseCoreplexConfig)

// class Sha3TestConfig extends Config()

class WithAdamAcc extends Config((site, here, up) => {
  case TileKey => RocketTileParams(
    icache = None,
    dcache = None,
    btb = None
  )

  case ProducerBuffers => Map()
  case ConsumerBuffers => Map()

  case ModularSystemsKey => Seq()
})

