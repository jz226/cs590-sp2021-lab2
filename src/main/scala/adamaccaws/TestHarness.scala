/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamaccaws

import chisel3._
import java.io.{File,FileWriter}
import freechips.rocketchip.diplomacy.LazyModule
import freechips.rocketchip.config.{Config, Field, Parameters}
import freechips.rocketchip.coreplex._
import freechips.rocketchip.amba.axi4._
import testchipip.GeneratorApp

import adamacc._

class TestHarness(implicit val p: Parameters) extends Module {

  val dut = Module(LazyModule(new AdamaccAWSTop()(p)).module)

  val io = IO(new Bundle {
    val ocl = chiselTypeOf(dut.ocl)
  }) 

  dut.ocl <> io.ocl

  // connect sim axi mem to ddr channels
  val nMemChannels = p(NMemChan)
  val sim_mem = LazyModule(new SimAXIMem(nMemChannels, 0x400000)(p))
  Module(sim_mem.module).io.axi4 <> dut.axi4_mem

}

object Generator extends GeneratorApp {
  generateFirrtl
}
