/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamaccaws

import chisel3._
import chisel3.util._

import java.io.{File, FileWriter}

import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.util._
import freechips.rocketchip.tile._
import freechips.rocketchip.config.{Parameters, Field, Config}
import freechips.rocketchip.devices.tilelink._
import freechips.rocketchip.coreplex._

import testchipip._
import adamacc._
import adamacc.common._

class AdamaccAWSTop(implicit p: Parameters) extends LazyModule {

  private val externalMemParams = p(ExtMem)
  private val lineSize = p(CacheBlockBytes)
  private val nMemChannels = p(NMemChan)
  private val device = new MemoryDevice

  // AXI-L Port
  val ocl_port = AXI4MasterNode(Seq(AXI4MasterPortParameters(
    masters = Seq(AXI4MasterParameters(
      name = "ocl",
      aligned = false, // could be true?
      maxFlight = Some(1)
    )),
    userBits = 0)
  ))

  // AXI4 DRAM Ports
  val axi4_ports = AXI4SlaveNode(Seq.tabulate(nMemChannels) { channel =>
    val base = AddressSet(externalMemParams.base, externalMemParams.size - 1)
    val filter = AddressSet(channel * lineSize, ~((nMemChannels - 1) * lineSize))

    AXI4SlavePortParameters(
      slaves = Seq(AXI4SlaveParameters(
        address = base.intersect(filter).toList,
        resources = device.reg,
        regionType = RegionType.UNCACHED,
        supportsWrite = TransferSizes(1, lineSize),
        supportsRead = TransferSizes(1, lineSize),
        interleavedId = Some(1)
      )),
      beatBytes = externalMemParams.beatBytes)
  })

  val adamacc = LazyModule(new AdamAccSystem()(p))

  // note dummyTL doesn't do it anything. it is to avoid rocket compile errors
  val dummyTL = p.alterPartial({
    case SharedMemoryTLEdge => adamacc.mem.head.edges.out.head
  })

  adamacc.mem.foreach { case m =>
    (axi4_ports
      := AXI4Buffer()
      := AXI4UserYanker()
      //:= AXI4IdIndexer(idBits = 9)
      := TLToAXI4()
      := TLBuffer()
      := TLWidthWidget(32)
      := m)
  }
  //look at aw and ar, w and r

  val axil_hub = LazyModule(new AXILHub()(dummyTL))

  // connect axil hub to external axil port
  axil_hub.node := ocl_port
  // connect axil hub to accelerator

  (adamacc.hostmem
    := TLFIFOFixer()
    := TLBuffer()
    := TLWidthWidget(4) // axil hub width = 4 bytes, adamacc width = 32 bytes
    := TLBuffer()
    := AXI4ToTL()
    //:= AXI4UserYanker()
    //:= AXI4Fragmenter()
    //:= AXI4IdIndexer(idBits = log2Ceil(8))
    := axil_hub.mem_out)

  lazy val module = new LazyModuleImp(this) {

    adamacc.module.io.cmd <> axil_hub.module.io.rocc_in
    axil_hub.module.io.rocc_out <> adamacc.module.io.resp

    val ocl = IO(HeterogeneousBag.fromNode(ocl_port.out).flip)
    (ocl zip ocl_port.out) foreach { case (o, (i, _)) => i <> o }

    val axi4_mem = IO(HeterogeneousBag.fromNode(axi4_ports.in))
    (axi4_mem zip axi4_ports.in) foreach { case (i, (o, _)) => i <> o }

    //add thing to here
    val arCnt = RegInit(0.U(64.W))
    val awCnt = RegInit(0.U(64.W))
    val rCnt = RegInit(0.U(64.W))
    val wCnt = RegInit(0.U(64.W))
    val bCnt = RegInit(0.U(64.W))
    when(axi4_mem(0).ar.fire()) {
      arCnt := arCnt + 1.U
    }
    when(axi4_mem(0).aw.fire()) {
      awCnt := awCnt + 1.U
    }
    when(axi4_mem(0).r.fire()) {
      rCnt := rCnt + 1.U
    }
    when(axi4_mem(0).w.fire()) {
      wCnt := wCnt + 1.U
    }
    when(axi4_mem(0).b.fire()) {
      bCnt := bCnt + 1.U
    }

    val rWait = RegInit(0.U(64.W))
    val bWait = RegInit(0.U(64.W))
    when(axi4_mem(0).r.ready && !axi4_mem(0).r.valid) {
      rWait := rWait + 1.U
    }

    when(axi4_mem(0).b.ready && !axi4_mem(0).b.valid) {
      bWait := bWait + 1.U
    }

    switch(adamacc.module.io.resp.bits.rd) {
      is(16.U) {
        axil_hub.module.io.rocc_out.bits.data := arCnt
      }
      is(17.U) {
        axil_hub.module.io.rocc_out.bits.data := awCnt
      }
      is(18.U) {
        axil_hub.module.io.rocc_out.bits.data := rCnt
      }
      is(19.U) {
        axil_hub.module.io.rocc_out.bits.data := wCnt
      }
      is(20.U) {
        axil_hub.module.io.rocc_out.bits.data := bCnt
      }
      is(21.U) {
        axil_hub.module.io.rocc_out.bits.data := rWait
      }
      is(22.U) {
        axil_hub.module.io.rocc_out.bits.data := bWait
      }
    }
  }

}

