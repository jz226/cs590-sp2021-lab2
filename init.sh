VCS_SETUP=/usr/project/lisa/apex/synopsys-setup-bash.sh
echo "test -f $VCS_SETUP && source $VCS_SETUP" > setup.sh
AWS_SETUP=/usr/project/lisa/apex/aws-setup-bash.sh
echo "test -f $AWS_SETUP && source $AWS_SETUP" >> setup.sh
CL_DIR=`pwd`/aws-fpga-adamacc/hdk/cl/developer_designs/adamacc
echo "export CL_DIR=$CL_DIR" >> setup.sh
echo "export PROJ_DIR=`pwd`" >> setup.sh

# git submodule update --init aws-fpga-adamacc
git clone https://gitlab.oit.duke.edu/jz226/aws-fpga-adamacc.git

# git submodule update --init testchipip
git clone https://github.com/ucb-bar/testchipip.git 
cd testchipip
git checkout e382fb4
cd ..

# git submodule update --init rocket-chip
git clone https://github.com/chipsalliance/rocket-chip.git
cd rocket-chip
git checkout 8799508
git apply ../rocketpatch.diff

git submodule update --init hardfloat
git submodule update --init firrtl
git submodule update --init chisel3

cd firrtl
sbt publishLocal
cd ../chisel3
sbt publishLocal

