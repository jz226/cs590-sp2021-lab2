val defaultVersions = Map(
  "chisel3" -> "3.1-SNAPSHOT",
  //  "chisel-iotesters" -> "1.2-SNAPSHOT",
  //  "firrtl-interpreter" -> "1.1-SNAPSHOT",
  "firrtl" -> "1.1-SNAPSHOT",
  "dsptools" -> "1.1.5"
)

lazy val commonSettings = Seq(
  scalaVersion := "2.11.7",
  scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked", "-language:reflectiveCalls"),
  libraryDependencies ++= (
    Seq("chisel3", "firrtl", "dsptools") map { dep: String =>
      "edu.berkeley.cs" %% dep % sys.props.getOrElse(dep + "Version", defaultVersions(dep))
    }

    ),

  resolvers ++= Seq(
    Resolver.sonatypeRepo("snapshots"),
    Resolver.sonatypeRepo("releases"),
    Resolver.mavenLocal
  )
)

lazy val rootSettings = commonSettings ++ Seq(
  organization := "edu.berkeley.cs",
  name := "adamaccaws",
  version := "1.0"
)

lazy val adamacc = project in file("adamacc") settings commonSettings
lazy val root = project in file(".") settings rootSettings dependsOn (adamacc)

