/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamacc.common

import adamacc.{DataChannelIO, ModularCoreIO}
import chisel3._
import chisel3.util._
import freechips.rocketchip.util._
import freechips.rocketchip.config.Parameters

/**
  * A small register-based memory providing an easy interface for setting
  * control information.
  *
  * @param n The number of registers
  * @param w The number of bits for each register
  */
class SettingsFile(n: Int, w: Int) extends Module {
  val io = IO(new Bundle {
    // Interface to write into the file
    val write = new Bundle {
      val en = Input(Bool())
      val addr = Input(UInt(log2Up(n).W))
      val data = Input(UInt(w.W))
    }
    // The values contained in the register file
    val values = Output(Vec(n, UInt(w.W)))
  })

  val values = Reg(Vec(n, UInt(w.W)))

  when(io.write.en) {
    values(io.write.addr) := io.write.data
  }

  io.values := values
}

/**
  * Routes data from the input to one or more outputs based on an ID
  *
  * @param typ   The Chisel Data type to route
  * @param n     Number of output interfaces
  * @param getId Function that takes the input data and produces the id
  */
class RequestRouter[T <: Data](typ: T, n: Int, getId: T => UInt) extends Module {
  val io = IO(new Bundle {
    val in = Flipped(Decoupled(typ))
    val out = Vec(n, Decoupled(typ))
  })

  val id = getId(io.in.bits)
  io.in.ready := io.out(id).ready

  io.out.zipWithIndex.foreach { case (out, i) =>
    out.valid := id === i.U && io.in.valid
    out.bits := io.in.bits
  }
}


object Util {
  def scaledLength(length: UInt, align: Int): UInt = {
    val l2 = log2Ceil(align)
    val needtoaddone = length(l2 - 1, 0).orR
    val lengthdiv = Mux(needtoaddone, 1.U + (length >> l2).asTypeOf(UInt()), length >> l2)
    //val ret = Cat(lengthdiv, 0.U(l2.W))
    Cat(lengthdiv)
  }

  /**
    * Enum for the local things with the Modular System (doesn't start at zero)
    *
    * @param n
    * @return
    */
  def LocalEnum(n: Int): Seq[UInt] = {
    (2 until ((n - 2) + 2)).map(_.U((1 max log2Ceil(n)).W)).toList
  }

  implicit class BoolSeqHelper(s: Seq[Bool]) {
    def any: Bool = s.reduce(_ || _)

    def none: Bool = !s.any



    def all: Bool = s.reduce(_ && _)

    def notAll: Bool = !s.all
  }

}

