/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamacc

import chisel3._
import chisel3.util._
import chisel3.experimental.MultiIOModule

import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.tile._
import freechips.rocketchip.coreplex.CacheBlockBytes
import freechips.rocketchip.util._
import freechips.rocketchip.config.{Parameters, Field}

class MaskTLRAM(
                   address: AddressSet,
                   cacheable: Boolean = true,
                   executable: Boolean = true,
                   beatBytes: Int = 32,
                   devName: Option[String] = None)
                 (implicit p: Parameters) extends LazyModule {

  val device = devName
    .map(new SimpleDevice(_, Seq("sifive,sram0")))
    .getOrElse(new MemoryDevice())

  val node = TLManagerNode(Seq(TLManagerPortParameters(
    Seq(TLManagerParameters(
      address = List(address),
      resources = device.reg("mem"),
      regionType = if (cacheable) RegionType.UNCACHED else RegionType.UNCACHEABLE,
      executable = executable,
      supportsGet = TransferSizes(1, beatBytes),
      supportsPutPartial = TransferSizes(1, beatBytes),
      supportsPutFull = TransferSizes(1, beatBytes),
      fifoId = Some(0))), // requests are handled in order
    beatBytes = beatBytes,
    minLatency = 1))) // no bypass needed for this device

  val resources = device.reg("mem")

  def bigBits(x: BigInt, tail: List[Boolean] = Nil): List[Boolean] =
    if (x == 0) tail.reverse else bigBits(x >> 1, ((x & 1) == 1) :: tail)

  def mask: List[Boolean] = bigBits(address.mask >> log2Ceil(beatBytes))

  // Use single-ported memory with byte-write enable
  def makeSinglePortedByteWriteSeqMem(size: Int) = {
    // We require the address range to include an entire beat (for the write mask)
    require((address.mask & (beatBytes - 1)) == beatBytes - 1)
    //val mem = SeqMem(size, Vec(beatBytes, Bits(width = 8)))
    val mem = SeqMem(size, Vec(beatBytes/4, UInt(32.W)))
    devName.foreach(n => mem.suggestName(n.split("-").last))
    mem
  }

  lazy val module = new LazyModuleImp(this) {
    val (in, edge) = node.in(0)

    val addrBits = (mask zip edge.addr_hi(in.a.bits).toBools).filter(_._1).map(_._2)
    val a_legal = address.contains(in.a.bits.address)
    val memAddress = Cat(addrBits.reverse)
    val mem = makeSinglePortedByteWriteSeqMem(1 << addrBits.size)

    val s_idle :: s_read :: s_write :: Nil = Enum(3)

    //val d_full = RegInit(Bool(false))
    val d_read = Reg(Bool())
    val d_size = Reg(UInt())
    val d_source = Reg(UInt())
    val d_data = Wire(UInt())
    val d_legal = Reg(Bool())

    val state = RegInit(s_idle)

    // Flow control
    when(in.d.fire()) {
      state := s_idle
    }
    //when (in.a.fire()) { d_full := Bool(true)  }
    in.d.valid := state === s_read || state === s_write
    in.a.ready := state === s_idle

    in.d.bits := edge.AccessAck(d_source, d_size, !d_legal)
    // avoid data-bus Mux
    in.d.bits.data := d_data
    in.d.bits.opcode := Mux(d_read, TLMessages.AccessAckData, TLMessages.AccessAck)

    val read = in.a.bits.opcode === TLMessages.Get
    val rdata = Wire(Vec(beatBytes/4, UInt(32.W)))
    val wmask = RegNext(FillInterleaved(8, in.a.bits.mask))
    val wdata = RegNext(in.a.bits.data)
    d_data := rdata.asTypeOf(UInt((beatBytes * 8).W))
    when(in.a.fire()) {
      d_read := read
      d_size := in.a.bits.size
      d_source := in.a.bits.source
      d_legal := a_legal
    }

    // exactly this pattern is required to get a RWM memory
    when(in.a.fire() && !read && a_legal) {
      state := s_write
      //mem.write(memAddress, wdata)
    }

    when (state === s_write && RegNext(in.a.fire())) {

      val wmaskdata = wmask & wdata
      val olddata = rdata.asTypeOf(UInt((beatBytes * 8).W)) & ~wmask
      mem.write(RegNext(memAddress), (wmaskdata | olddata).asTypeOf(Vec(beatBytes/4, UInt(32.W))))
      //state := s_idle
    }

    when(in.a.fire() && read) {
      state := s_read
    }

    val ren = in.a.fire()// && read
    rdata := mem.readAndHold(memAddress, ren)

    // Tie off unused channels
    in.b.valid := Bool(false)
    in.c.ready := Bool(true)
    in.e.ready := Bool(true)
  }
}
