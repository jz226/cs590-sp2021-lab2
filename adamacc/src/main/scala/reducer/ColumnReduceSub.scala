/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamacc.reducer

import chisel3._
import chisel3.util._
import freechips.rocketchip.config.{Field, Parameters}
import freechips.rocketchip.tile.RoCCResponse
import freechips.rocketchip.util._


class ColumnReduceSubCoreRequest() extends Bundle {
  val id = UInt(5.W) // max 32 cores
  val rd = UInt(5.W) // 5 bits allocated for rd

  /** The type of reduction to do
   * 0 - sum
   * 1 - minimum
   * 2 - maximum
   */
  val reduceOp = UInt(2.W)
  /**
   * The type of comparison to do.
   * 0 - equal to
   * 1 - less than
   * 2 - less than or equal to
   * 3 - greater than
   * 4 - greater than or equal to
   */
  val compOp = UInt(3.W)
  /** The value to compare each byte to */
  val compVal = UInt(8.W)
  val inputLen = UInt(10.W)
  val totalLines = UInt(32.W)
  // val threshold = UInt(32.W)
}

class ColumnReduceSubCoreResponse() extends Bundle  {
  val id = UInt(5.W) // max 32 cores
  val rd = UInt(5.W) // 5 bits allocated for rd
  val error = UInt(32.W)
}

class ColumnReduceSubCoreIO(inputBytes : Int, outputBytes : Int) extends Bundle {
  val req = Flipped(Decoupled(new ColumnReduceSubCoreRequest))
  val resp = Decoupled(new ColumnReduceSubCoreResponse)
  val busy = Output(Bool())
  val input = Flipped(new tempIO(inputBytes * 8))
  val output = new tempIO(outputBytes * 8)
  override def cloneType = new ColumnReduceSubCoreIO(inputBytes, outputBytes).asInstanceOf[this.type]
}

class tempIO(maxBits: Int) extends Bundle {
  val data = Decoupled(UInt(maxBits.W))
  val stop = Input(Bool())
  val finished = Output(Bool())
  override def cloneType = new tempIO(maxBits).asInstanceOf[this.type]
}

class ColumnReduceSubCore(inputBytes : Int, outputBytes : Int) extends Module {
  val io = IO(new ColumnReduceSubCoreIO(inputBytes, outputBytes))

  val reqCache = Reg(new ColumnReduceSubCoreRequest())
  val respCache = Reg(new ColumnReduceSubCoreResponse())

  val state_test = RegInit(VecInit(Seq.fill(6)(0.U(30.W))))
  val s_idle :: s_req :: s_calc :: s_end :: s_finishing :: s_error :: Nil = Enum(6)
  val state = RegInit(init = s_idle)
  val reduceOp = Reg(UInt(2.W))
  val compOp = Reg(UInt(3.W))
  val compVal = Reg(UInt(8.W))
  val inputLen = Reg(UInt(10.W))
  val totalLines = Reg(UInt(32.W))
  val lineNum = RegInit(0.U(32.W))
  val data = Reg(Vec(inputBytes, UInt(8.W)))
  val input_available = RegInit(false.B)
  val accum = RegInit(0.U((outputBytes*8).W))
  val idx = Reg(UInt(10.W))
  val inputIdx = Reg(UInt(10.W))
  val ct = Reg(UInt(5.W))
  val last_state = RegInit(0.U(3.W))
  val debug = RegInit(0.U(3.W))

  io.req.ready := false.B
  io.input.stop := false.B
  io.input.data.ready := false.B
  input_available := false.B
  // io.output
  io.output.data.valid := false.B
  io.output.data.bits := accum 
  io.output.finished := false.B
  // io.resp
  io.resp.bits.id := respCache.id
  io.resp.bits.rd := respCache.rd
  io.resp.bits.error := respCache.error
  io.resp.valid := false.B
  // io.busy
  io.busy := state =/= s_idle

  // io.req
  when (state === s_idle) {
    for (i <- 1 until 6) {
      state_test(i) := 0.U
    }
    io.req.ready := true.B
    when (io.req.fire()) {
      last_state := 7.U
      reqCache := io.req.bits
      respCache.id := io.req.bits.id
      respCache.rd := io.req.bits.rd
      reduceOp := io.req.bits.reduceOp
      compOp := io.req.bits.compOp
      compVal := io.req.bits.compVal
      inputLen := io.req.bits.inputLen
      totalLines := io.req.bits.totalLines
      respCache.error := Cat(7.U, 4.U, last_state, inputLen(4, 0), totalLines(4, 0), reduceOp, compOp, compVal)  // 3, 3, 3, 5, 5, 2, 3, 8
      state := s_req
    }

    idx := 0.U
    inputIdx := 0.U
    ct := 4.U
    accum := 0.U
  }

  val data_valid = Wire(Vec(inputBytes, Bool()))
  data_valid.foreach(_ := false.B)

  val first_char = Reg(UInt(8.W))
  val qual1 = RegInit(VecInit(Seq.fill(inputBytes/2)(0.U(9.W))))
  val qual2 = RegInit(VecInit(Seq.fill(inputBytes/4)(0.U(10.W))))
  val qual3 = RegInit(VecInit(Seq.fill(inputBytes/8)(0.U(11.W))))
  val qual4 = RegInit(VecInit(Seq.fill(inputBytes/16)(0.U(12.W))))

  when (state === s_req || state === s_calc) {
    when (state === s_req) {
      last_state := s_req
    } .otherwise {
      last_state := s_calc
    }
    io.input.data.ready := inputIdx < inputLen
    when (io.input.data.fire()) {
      data := (io.input.data.bits).asTypeOf(data)

      inputIdx := inputIdx + inputBytes.asUInt()
      input_available := true.B
      state := s_calc
      when(inputIdx === 0.U) {
        val data_temp = Wire(Vec(inputBytes, UInt(8.W)))
        data_temp := (io.input.data.bits).asTypeOf(data_temp)
        when (idx < inputLen) {
          data_temp.zipWithIndex.foreach {
            case (data_i, i) => val compOK = MuxLookup(compOp, false.B, Seq(
              0.U -> (data_i === compVal),
              1.U -> (data_i < compVal),
              2.U -> (data_i <= compVal),
              3.U -> (data_i > compVal),
              4.U -> (data_i >= compVal)))
            when(compOK) {
              data_valid(i) := true.B
            }
          }
          idx := idx + inputBytes.asUInt()
        }

        for (i <- 0 until inputBytes/2) {
          qual1(i) := Mux(data_valid(i * 2), data_temp(i * 2), 0.U) +& Mux(data_valid(i * 2 + 1), data_temp(i * 2 + 1), 0.U)
        }
        input_available := false.B
      }
    }
  }

  when (state === s_calc) {
    last_state := s_calc
    val qual5 = Wire(UInt(13.W))
    qual5 := 0.U
    when (input_available) {
      first_char := data(0)
      when (idx < inputLen) {
        data.zipWithIndex.foreach {
          case (data_i, i) => val compOK = MuxLookup(compOp, false.B, Seq(
            0.U -> (data_i === compVal),
            1.U -> (data_i < compVal),
            2.U -> (data_i <= compVal),
            3.U -> (data_i > compVal),
            4.U -> (data_i >= compVal)))
          when(compOK) {
            data_valid(i) := true.B
          }
        }
        idx := idx + inputBytes.asUInt()
      }
    }
    for (i <- 0 until inputBytes/2) {
      qual1(i) := Mux(data_valid(i * 2), data(i * 2), 0.U) +& Mux(data_valid(i * 2 + 1), data(i * 2 + 1), 0.U)
    }
    for (i <- 0 until inputBytes/4) {
      qual2(i) := qual1(i * 2) +& qual1(i * 2 + 1)
    }
    for (i <- 0 until inputBytes/8) {
      qual3(i) := qual2(i * 2) +& qual2(i * 2 + 1)
    }
    for (i <- 0 until inputBytes/16) {
      qual4(i) := qual3(i * 2) +& qual3(i * 2 + 1)
    }
    qual5 := qual4(0) +& qual4(1)
    accum := accum +& qual5

    when (idx  === inputLen) {
      ct := ct - 1.U
      when (ct === 0.U) {
        state := s_end
        lineNum := lineNum + 1.U
      }
    }  
  }

  when (state === s_end) {
    last_state := s_end
    io.output.data.valid := !io.output.stop
    io.output.data.bits := accum
    when(io.output.data.fire()) {
      respCache.error := 13.U
      when(lineNum === totalLines || lineNum >= totalLines) {
        state := s_finishing  
        respCache.error := Cat(17.U, 0.U, debug)
      } .otherwise {
        idx := 0.U
        inputIdx := 0.U
        ct := 4.U
        accum := 0.U
        state := s_req
      }
    }
  }

  when(state === s_finishing) {
    last_state := s_finishing
    when(!io.input.finished) {
      debug := 1.U
    }
    io.output.finished := true.B
    io.resp.valid := true.B

    when (io.resp.fire()) { 
      state := s_idle 
    }
  }
}
