/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamacc.reducer

import chisel3._
import chisel3.util._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.tile._
import freechips.rocketchip.coreplex.CacheBlockBytes
import freechips.rocketchip.util._
import freechips.rocketchip.config.{Parameters, Field}
import adamacc._
import adamacc.common._
import adamacc.common.Util._

case class ColumnReducerCoreParams(
                                 nMemXacts: Int, // not for production release
                                 readElementSizes: Seq[Int], // size in bytes
                                 writeElementSizes: Seq[Int], // size in bytes
                                 readLocations: Option[Seq[String]] = None,
                                 writeLocations: Option[Seq[String]] = None,
                                 inputBytes: Int,
                                 outputBytes: Int
                               ) extends ModularCoreParams

/**
 * @inputBytes the maximum number of bytes in the input
 * @outputBytes the maximum number of bytes in the output
 * @nUnits the number of independent units
 */
case class ColumnReducerConfig(
  coreParams : ColumnReducerCoreParams,
  opcode : Int,
  nCores: Int)
 extends ModularSystemParams {
  override def buildSystem(implicit p: Parameters): ModularSystem = new ColumnReducerSystem(this)
}

case object ColumnReducerKey extends Field[Option[ColumnReducerConfig]]

class ColumnReduceCoreRequest(modularInterface : ColumnReducerCoreParams)(implicit p: Parameters) extends ModularCoreBundle()(p) {
  /** The type of reduction to do
   * 0 - sum
   * 1 - minimum
   * 2 - maximum
   */
  val reduceOp = UInt(2.W)
  /**
   * The type of comparison to do.
   * 0 - equal to
   * 1 - less than
   * 2 - less than or equal to
   * 3 - greater than
   * 4 - greater than or equal to
   */
  val compOp = UInt(3.W)
  /** The value to compare each byte to */
  val compVal = UInt(8.W)
  val inputLen = UInt(10.W)
  val totalLines = UInt(32.W)
  override def cloneType = new ColumnReduceCoreRequest(modularInterface).asInstanceOf[this.type]
}

class ColumnReduceCoreResponse()(implicit p: Parameters) extends ModularCoreBundle()(p){
  val error = UInt(32.W)
  override def cloneType = new ColumnReduceCoreResponse().asInstanceOf[this.type]
}

class ColumnReduceCoreIO(modularInterface : ColumnReducerCoreParams)(implicit p: Parameters) extends ModularCoreIO()(p) {
  val req = Flipped(Decoupled(new ColumnReduceCoreRequest(modularInterface)(p)))
  val resp = Decoupled(new ColumnReduceCoreResponse())
  override def cloneType = new ColumnReduceCoreIO(modularInterface).asInstanceOf[this.type]
}

class ColumnReduceCore(addressBits : Int, modularInterface : ColumnReducerCoreParams)(implicit p: Parameters) extends ModularCore(addressBits, modularInterface)(p) {
  val io = IO(new ColumnReduceCoreIO(modularInterface)(p))

  // (BEGIN)
  val inputBytes = modularInterface.inputBytes
  val outputBytes = modularInterface.outputBytes
  val outputBits = 8 * outputBytes
  val outputSizeBits = log2Up(log2Ceil(outputBytes) + 1)
  val pAddrBits = 32
  // (END)

  val subcore = Module(new ColumnReduceSubCore(inputBytes, outputBytes))
  subcore.io.input <> readChannels(0)
  writeChannels(0) <> subcore.io.output
  subcore.io.req <> io.req
  io.resp <> subcore.io.resp
  io.busy := subcore.io.busy
}

/*
 * Used for reducing the quality score of read into a single value
 * input should be a stream of 8-bit char's
 * outputs the value from internal accumulator
 * assumes a null terminator for the steam
 * able to compare against an input constant
 */
class ColumnReducerSystem(systemParams : ColumnReducerConfig)(implicit p: Parameters) extends ModularSystem(systemParams)(p) {
  // (BEGIN)
  val reduceExternal = systemParams.coreParams
  val inputBytes = reduceExternal.inputBytes
  val pAddrBits = 32
  // (END)

  lazy val module = new ModularSystemImp(this) {
    val io = IO(new ModularSystemIO)
    val cores = Seq.fill(nCores) { Module(new ColumnReduceCore(addressBits, systemParams.coreParams)(p)) }
    setupCode()
    val funct_comp :: funct_op :: funct_len :: Nil = LocalEnum(5)
    val reduceOp = Reg(Vec(nCores, UInt(2.W)))
    val compOp = Reg(Vec(nCores, UInt(3.W)))
    val compVal = Reg(Vec(nCores, UInt(8.W)))
    val inputLen = Reg(Vec(nCores, UInt(10.W)))
    val totalLines = Reg(Vec(nCores, UInt(32.W)))
    val numCores = RegInit(0.U(32.W))
    val responses = Reg(Vec(nCores, new ColumnReduceCoreResponse()))
    val resp_count = RegInit(0.U((log2Ceil(nCores)+1).W))
    val out_count = RegInit(0.U((log2Ceil(nCores)+1).W))
    val numCores_valid = RegInit(false.B)
    when (cmd.fire()) {
      switch (funct) {
        is (funct_comp) {
          compOp(coreSelect) := cmd.bits.rs1
          compVal(coreSelect) := cmd.bits.rs2
        }
        is (funct_op) {
          reduceOp(coreSelect) := cmd.bits.rs1
          inputLen(coreSelect) := cmd.bits.rs2
        }
        is (funct_len) {
          totalLines(coreSelect) := cmd.bits.rs1
          numCores := cmd.bits.rs2
          numCores_valid := true.B
        }
      }
    }

    cores.zipWithIndex.foreach { case(core, i) =>
      val cmdBitsLatch = RegNext(cmd.bits)
      val inputLength = cmdBitsLatch.rs2
      core.io.req.bits.reduceOp := reduceOp(i)
      core.io.req.bits.compOp := compOp(i)
      core.io.req.bits.compVal := compVal(i)
      core.io.req.bits.inputLen := inputLen(i)
      core.io.req.bits.totalLines := totalLines(i)
      readLengths(i).foreach(_ := inputLength)
      writeLengths(i).foreach(_ := totalLines(i))
    }

    val system_res_ready = Wire(Bool())
    val arbiter = Module(new RRArbiter(new ColumnReduceCoreResponse(), nCores))
    arbiter.io.in <> cores.map(_.io.resp)
    resp.valid := false.B
    resp.bits.rd := 0.U
    resp.bits.data := 0.U
    system_res_ready := true.B
    arbiter.io.out.ready := system_res_ready
    when(arbiter.io.out.fire()) {
      resp_count := resp_count + 1.U
      responses(resp_count) := arbiter.io.out.bits
    }

    when(numCores_valid && resp_count === numCores) {
      when(out_count < numCores && resp.ready) {
        resp.valid := true.B
        resp.bits.rd := (responses(out_count)).rd
        resp.bits.data := Cat((responses(out_count)).id, (responses(out_count)).error)
        out_count := out_count + 1.U
      } .elsewhen(out_count === numCores) {
        resp_count := 0.U
        out_count := 0.U
        numCores_valid := false.B
        numCores := 0.U
      }
    }
  }
}
