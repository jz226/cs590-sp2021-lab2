/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamacc

import chisel3.{util, _}
import chisel3.util._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.tile._
import freechips.rocketchip.coreplex.CacheBlockBytes
import freechips.rocketchip.util._
import freechips.rocketchip.config.Parameters
import adamacc.common._
import freechips.rocketchip.rocket.MStatus
import adamacc.common.Util._

import scala.language.postfixOps

class AdamAcc(implicit p: Parameters) extends LazyModule {

  val gconfigs: Seq[ModularSystemParams] = p(ModularSystemsKey)

  val configs = gconfigs
  val gsystems: Seq[ModularSystem] = configs.map { c =>
    LazyModule(c.buildSystem(p) /*c.associatedSystem.getConstructors()(0).newInstance(c, p).asInstanceOf[ModularSystem]*/)
  }

  def sysLookup(opcode: Int): ModularSystem = {
    gsystems(configs.indexWhere { ms =>
      ms.opcode == opcode
    })
  }

  configs.foreach { c => println("Opcode " + c.opcode + ": " + c.getClass.getCanonicalName) }

  val reserved_funct = 0.U

  val system_functs = configs.map(_.opcode.U)
  val systems = gsystems

  val memGroups = systems.map(_.mem).filter(_ nonEmpty)
  val mem = Seq.fill(memGroups.size + 1) {
    TLIdentityNode()
  }
  (mem.dropRight(1), memGroups).zipped.foreach { case (acc_channel, unit_channels) =>
    val usize = unit_channels.size
    println(s"unit channels size = $usize")
    if (unit_channels nonEmpty) {
      val arb = LazyModule(new TLXbar)
      unit_channels.foreach { c => arb.node := c }
      acc_channel := TLBuffer() := arb.node
    }
  }

  // type2type wiring
  val producerList = p(ProducerBuffers)
  for ((writelist, readlist) <- producerList) {

    // nWriteChannels x nReadChannels crossbar
    val arb = LazyModule(new TLXbar)
    writelist.foreach { case (write, chIdx) =>
      val lmem = sysLookup(write).localWrite(chIdx)
      if (lmem isEmpty) println("WRITE not defined " + write + " , " + chIdx)
      lmem.get := TLBuffer() := arb.node
    }
    readlist.foreach { case (read, chIdx) =>
      val rmem = sysLookup(read).remoteRead(chIdx)
      if (rmem isEmpty) println("READ not defined " + read + " , " + chIdx)
      arb.node := TLBuffer() := rmem.get
    }

  }
  val consumerList = p(ConsumerBuffers)
  for ((writelist, readlist) <- consumerList) {

    // nWriteChannels x nReadChannels crossbar
    val arb = LazyModule(new TLXbar)
    writelist.foreach { case (write, chIdx) =>
      val rmem = sysLookup(write).remoteWrite(chIdx)
      if (rmem isEmpty) println("WRITE not defined " + write + " , " + chIdx)
      arb.node := TLBuffer() := rmem.get
    }
    readlist.foreach { case (read, chIdx) =>
      val lmem = sysLookup(read).localRead(chIdx)
      if (lmem isEmpty) println("READ not defined " + read + " , " + chIdx)
      lmem.get := TLBuffer() := arb.node
    }

  }

  val roccReader = LazyModule(new ColumnReadChannel(32))
  mem.last := roccReader.node

  lazy val module = new LazyModuleImp(this) {
    val io = IO(new Bundle {
      val cmd = Flipped(Decoupled(new RoCCCommand))
      val resp = Decoupled(new RoCCResponse)
      val busy = Output(Bool())
      val partCounter = Output(UInt(32.W))
    })

    val cmd = Queue(io.cmd)
    //cmd.ready := false.B

    val memCmd = Wire(Decoupled(new RoCCCommand))
    //memCmd.bits := roccReader.module.io.data.bits/*(32 * 5 - 1,0)*/.asTypeOf(new RoCCCommand)
    memCmd.bits.inst := roccReader.module.io.channel.data.bits(31, 0).asTypeOf(new RoCCInstruction)
    memCmd.bits.inst.funct := roccReader.module.io.channel.data.bits(31, 25)
    memCmd.bits.rs1 := Cat(roccReader.module.io.channel.data.bits(32 * 2 - 1, 32 * 1), roccReader.module.io.channel.data.bits(32 * 3 - 1, 32 * 2))
    memCmd.bits.rs2 := Cat(roccReader.module.io.channel.data.bits(32 * 4 - 1, 32 * 3), roccReader.module.io.channel.data.bits(32 * 5 - 1, 32 * 4))
    memCmd.bits.status := 0.U.asTypeOf(new MStatus())
    memCmd.ready <> roccReader.module.io.channel.data.ready
    memCmd.valid <> roccReader.module.io.channel.data.valid
    val memCmdOut = Queue(memCmd)

    val maxCores = 32
    val maxTypes = 16
    val returnFile = Reg(Vec(maxTypes, Vec(maxCores, UInt(32.W))))

    val cmdArb = Module(new RRArbiter(new RoCCCommand, 2))
    cmdArb.io.in <> Seq(cmd, memCmdOut)

    // route cmds between:
    //  custom0: control opcodes (used for WAIT commands)
    //  custom3: accelerator commands
    val cmdRouter = Module(new RoccCommandRouter(Seq(OpcodeSet.custom0, OpcodeSet.custom3)))
    cmdRouter.io.in <> cmdArb.io.out

    val optionCmd = cmdRouter.io.out(0)

    val accCmd = cmdRouter.io.out(1)
    val funct = accCmd.bits.inst.funct(6, 3)
    accCmd.ready := false.B //base case

    roccReader.module.io.req.bits.addr := accCmd.bits.rs1
    roccReader.module.io.req.bits.len := accCmd.bits.rs2
    roccReader.module.io.req.valid := accCmd.valid && (funct === reserved_funct)
    when(funct === reserved_funct) {
      accCmd.ready := roccReader.module.io.req.ready
    }

    val respIF = systems.map(_.module.io.resp)
    val respArb = Module(new RRArbiter(new RoCCResponse, respIF.size))
    respArb.io.in <> respIF
    io.resp <> respArb.io.out

    when(respArb.io.out.fire()) {
      returnFile(respArb.io.out.bits.rd)(respArb.io.out.bits.data(63, 32)) := respArb.io.out.bits.data(31, 0)
    } //TODO: make sure other things are flopped, see MB

    // MONITOR CODE
    val busyFile = RegInit(VecInit(Seq.fill(maxTypes)(VecInit(Seq.fill(maxCores)(false.B)))))
    val hostRespFile = RegInit(VecInit(Seq.fill(maxTypes)(VecInit(Seq.fill(maxCores)(false.B)))))
    val monitorWaiting = RegInit(false.B)

    val systemQueues = systems.map(_ => Module(new Queue(new RoCCCommand, 2)))
    (systems, system_functs, systemQueues).zipped.foreach { case (s, c, q) =>
      q.io.enq.valid := (accCmd.valid && funct === c /* && !monitorWaiting*/)
      q.io.enq.bits := accCmd.bits
      when(funct === c) {
        accCmd.ready := q.io.enq.ready
      }
      s.module.io.cmd <> q.io.deq
      when(q.io.deq.bits.inst.xs1) {
        s.module.io.cmd.bits.rs1 := returnFile(q.io.deq.bits.rs1(63, 32))(q.io.deq.bits.rs1(31, 0))
      }
      when(q.io.deq.bits.inst.xs2) {
        s.module.io.cmd.bits.rs2 := returnFile(q.io.deq.bits.rs2(63, 32))(q.io.deq.bits.rs2(31, 0))
      }
      when(q.io.deq.fire() && isStart(q.io.deq.bits)) {
        busyFile(q.io.deq.bits.inst.rd)(q.io.deq.bits.inst.rs1) := true.B
        hostRespFile(q.io.deq.bits.inst.rd)(q.io.deq.bits.inst.rs1) := q.io.deq.bits.inst.xd
      }
      when(monitorWaiting) {
        q.io.deq.ready := false.B
        s.module.io.cmd.valid := false.B
      }
    }

    val anyBusy = busyFile.map(_.reduce(_ || _)).reduce(_ || _)
    when(monitorWaiting === true.B && !anyBusy) {
      monitorWaiting := false.B
    }
    when(optionCmd.fire()) {
      monitorWaiting := true.B
    }
    optionCmd.ready := !monitorWaiting

    def isStart(x: RoCCCommand): Bool = {
      x.inst.funct(6, 3) =/= reserved_funct && (x.inst.funct(2, 0) === 0.U || /*(x.inst.funct(6, 3) === 11.U && x.inst.funct(2, 0) === 2.U) ||*/ (x.inst.funct(6, 3) === 7.U && x.inst.funct(2, 0) === 3.U)) //TODO: Fix horrible hack for mb having two starts
    }

    when(respArb.io.out.fire()) {
      busyFile(respArb.io.out.bits.rd)(respArb.io.out.bits.data(63, 32)) := false.B
      hostRespFile(respArb.io.out.bits.rd)(respArb.io.out.bits.data(63, 32)) := false.B
    }

    io.resp.valid := respArb.io.out.valid && hostRespFile(respArb.io.out.bits.rd)(respArb.io.out.bits.data(63, 32))
    // MONITOR CODE END

    io.busy := cmdArb.io.out.valid || accCmd.valid || systems.map(_.module.io.busy).any || systemQueues.map(_.io.count =/= 0.U).any
  }

}

class AdamAccSystem(implicit p: Parameters) extends LazyModule {

  val nMemChannels = p(NMemChan)
  println(nMemChannels + " ddr channels")

  private val lineSize = p(CacheBlockBytes)

  val hostmem = TLIdentityNode()
  val mem = Seq.fill(nMemChannels) {
    TLIdentityNode()
  }

  val dummyTL = p.alterPartial({
    case SharedMemoryTLEdge => mem.head.edges.out.head
  })
  val acc = LazyModule(new AdamAcc()(dummyTL))

  val crossbar = LazyModule(new TLXbar)
  acc.mem.foreach {
    crossbar.node := _
  }
  crossbar.node := hostmem
  mem.foreach {
    _ := crossbar.node
  }

  lazy val module = new LazyModuleImp(this) {
    val io = IO(new Bundle {
      val cmd = Flipped(Decoupled(new RoCCCommand()(dummyTL)))
      val resp = Decoupled(new RoCCResponse()(dummyTL))
    })

    acc.module.io.cmd <> io.cmd
    io.resp <> acc.module.io.resp
  }

}
