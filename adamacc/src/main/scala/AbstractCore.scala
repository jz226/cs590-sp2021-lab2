/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package adamacc

import adamacc.common.plumbing.SFQueue
import chisel3._
import chisel3.util._
import chisel3.experimental.MultiIOModule
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.tile._
import freechips.rocketchip.coreplex.CacheBlockBytes
import freechips.rocketchip.util._
import freechips.rocketchip.config.{Field, Parameters}

// import adamacc.ir._
import adamacc.common._
import adamacc.common.Util._

// trait Channel {
//   val elementSize : Int
//   val location: String = "Mem"
// }

// To-Do:
// extend Field w/ a "ConfigKey" trait
// seal that trait (restricts subclasses to one file)
// then loop w/ case statements over the subclasses
// which binds keys to constructor statements
//
// make an object ConfigKey that has:
// a "build" function that returns a ModularSystem
// whenever you make a new Key, update the build function
// and the "keys" Seq
//
// loop over "keys" and call "build" on each of them

// TO-DO:
// use the require statement in the case class of this trait
// to enforce e.g. "two more outputs than input channel"
// TRY:
// replace val (here) with def -> val only in concrete case class
// TO-DO: instead of size seq + location seq
//        use a seq of a trait (which defines all the parts of a channel)
trait ModularCoreParams {
  val nMemXacts: Int // not for production release
  //  val readChannels: Seq[Channel]
  //  val writeChannels: Seq[Channel]
  val readElementSizes: Seq[Int] // size in bytes
  val writeElementSizes: Seq[Int] // size in bytes
  val readLocations: Option[Seq[String]]
  val writeLocations: Option[Seq[String]]

  val customRead: Boolean = false
}

case object ModularSystemsKey extends Field[Seq[ModularSystemParams]]


trait ModularSystemParams {
  val coreParams: ModularCoreParams
  val nCores: Int
  val opcode: Int

  def buildSystem(implicit p: Parameters): ModularSystem
  //val associatedSystem: Class[_ <: ModularSystem] = classOf[ModularSystem]
  /**
    * In elements, per write channel, scaled by the number of bytes
    */
  val bufSize: Int = 1024
  val bufMasked: Boolean = false
  val doubleBuf: Boolean = false
  val channelQueueDepth: Int = 32
}

abstract class ModularCoreBundle(implicit val p: Parameters) extends ParameterizedBundle()(p) {
  //with HasCoreParams
  val id = Input(UInt(5.W)) // max 32 cores
  val rd = Input(UInt(5.W)) // 5 bits allocated for rd
}

abstract class ModularCoreIO(implicit p: Parameters) extends ParameterizedBundle()(p) {
  val req: DecoupledIO[ModularCoreBundle]
  val resp: DecoupledIO[ModularCoreBundle]
  val busy = Output(Bool())
}

class DataChannelIO(maxBytes: Int) extends Bundle {
  val data = Decoupled(UInt((maxBytes * 8).W))
  val stop = Input(Bool())
  val finished = Output(Bool())

  override def cloneType = new DataChannelIO(maxBytes).asInstanceOf[this.type]
}

abstract class ModularCore(addressBits: Int, val modularInterface: ModularCoreParams)(implicit p: Parameters)
  extends MultiIOModule { // with HasCoreParams
  val io: ModularCoreIO

  val readChannels = if(!modularInterface.customRead) {
    modularInterface.readElementSizes.map {
      rSize => IO(Flipped(new DataChannelIO(rSize)))
    }} else {
    Seq()
  }

  val writeChannels =
    modularInterface.writeElementSizes.map {
      wSize => IO(new DataChannelIO(wSize))
    }

}

class ModularSystemIO(implicit p: Parameters) extends Bundle {
  val cmd = Flipped(Decoupled(new RoCCCommand))
  val resp = Decoupled(new RoCCResponse)
  val busy = Output(Bool())
}

abstract class ModularSystem(val systemParams: ModularSystemParams)(implicit p: Parameters) extends LazyModule {
  val nCores = systemParams.nCores
  val queueDepth = systemParams.channelQueueDepth
  val modularInterface = systemParams.coreParams
  val opcode = systemParams.opcode

  // replace this code with "getorelse"
  val readLoc = modularInterface.readLocations.getOrElse(Seq.fill(modularInterface.readElementSizes.length) {
    "Mem"
  })
  val writeLoc = modularInterface.writeLocations.getOrElse(Seq.fill(modularInterface.writeElementSizes.length) {
    "Mem"
  })

  val hasMem = readLoc.map { case rloc =>
    rloc contains "Mem"
  }.reduce(_ || _) || writeLoc.map { case wloc =>
    wloc contains "Mem"
  }.reduce(_ || _)
  val mem = if (hasMem) {
    Seq.fill(nCores) {
      TLIdentityNode()
    }
  } else {
    Seq()
  }

  val readers = Seq.fill(nCores) {
    modularInterface.readElementSizes.zip(readLoc).map { case (nBytes, rloc) =>
      LazyModule(new ColumnReadChannel(nBytes)(p))
    }
  }
  val writers = Seq.fill(nCores) {
    modularInterface.writeElementSizes.zip(writeLoc).map { case (nBytes, wloc) =>
      LazyModule(new FixedSequentialWriteChannel(nBytes, modularInterface.nMemXacts)(p))
    }
  }

  val maxBufSize = 1024 * 4 * 4
  val maxCores = 32
  val maxWriteCount = 4
  val maxReadCount = 5
  val maxMemAddr = 0x400000000L
  val bufStride = 1 // used for sharing a buffer amongst several cores

  val producerBuffers = modularInterface.writeElementSizes.zipWithIndex.map { case (nBytes, i) =>
    val wloc = writeLoc(i)
    if (wloc contains "Local") {
      val bufSize = /*systemParams.bufSize  * nBytes*/ maxBufSize * bufStride

      Some((0 until nCores by bufStride).map { case x =>
        val bufAddr = (opcode * maxWriteCount * maxCores * maxBufSize) + (i * maxCores * maxBufSize) + (x * maxBufSize) + maxMemAddr
        if (systemParams.bufMasked) {
          LazyModule(new MaskTLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32)).node
          //LazyModule(new TLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32)).node
        } else {
          LazyModule(new SimpleTLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32)).node
        }
      })
    } else {
      None
    }
  }

  val consumerBuffersModules = modularInterface.readElementSizes.zipWithIndex.map { case (nBytes, i) =>
    val rloc = readLoc(i)
    if (rloc contains "Local") {
      val bufSize = /*systemParams.bufSize  * nBytes*/ maxBufSize * bufStride
      //TODO: This address thing sucks, can collide addresses between the two
      Some((0 until nCores by bufStride).map { case x =>
        val bufAddr = (opcode * maxReadCount * maxCores * maxBufSize) + (i * maxCores * maxBufSize) + (x * maxBufSize) + maxMemAddr
        if (systemParams.bufMasked) {
          LazyModule(new MaskTLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32))
          //LazyModule(new TLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32)).node
        } else if (systemParams.doubleBuf) {
          LazyModule(new DoubleTLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32))
        } else {
          LazyModule(new SimpleTLRAM(AddressSet(bufAddr, bufSize - 1), beatBytes = 32))
        }
      })
    } else {
      None
    }
  }
  val consumerBuffers = consumerBuffersModules.map { case bufs =>
    if (bufs.isDefined) {
      if (systemParams.bufMasked) {
        Some(bufs.get.map(_.asInstanceOf[MaskTLRAM].node))
      } else if (systemParams.doubleBuf) {
        Some(bufs.get.map(_.asInstanceOf[DoubleTLRAM].node))
      } else {
        Some(bufs.get.map(_.asInstanceOf[SimpleTLRAM].node))
      }
    } else {
      None
    }
  }

  // potentially split writer node
  // (mem, localBuf, remoteBuf)
  val writeSplit = writeLoc.zipWithIndex.map { case (wloc, i) =>
    writers.zipWithIndex.map { case (wlist, coreid) =>
      // wlist(i) is one writer
      if ((wloc contains "Local") && (wloc contains "Mem")) {
        val split = LazyModule(new TLXbar)
        split.node := wlist(i).node
        (Some(split.node), Some(split.node), None)
      } else if ((wloc contains "Remote") && (wloc contains "Mem")) {
        val split = LazyModule(new TLXbar)
        split.node := wlist(i).node
        (Some(split.node), None, Some(split.node))
      } else if (wloc contains "Mem") {
        (Some(wlist(i).node), None, None)
      } else if (wloc contains "Local") {
        (None, Some(wlist(i).node), None)
      } else if (wloc contains "Remote") {
        (None, None, Some(wlist(i).node))
      } else {
        (None, None, None)
      }
    }
  }

  // potentially split reader node
  // (mem, localBuf, remoteBuf)
  val readSplit = readLoc.zipWithIndex.map { case (rloc, i) =>
    readers.zipWithIndex.map { case (rlist, coreid) =>
      // rlist(i) is one reader
      if ((rloc contains "Remote") && (rloc contains "Mem")) {
        val split = LazyModule(new TLXbar)
        split.node := rlist(i).node
        (Some(split.node), None, Some(split.node))
      } else if ((rloc contains "Local") && (rloc contains "Mem")) {
        val split = LazyModule(new TLXbar)
        split.node := rlist(i).node
        (Some(split.node), Some(split.node), None)
      } else if (rloc contains "Mem") {
        (Some(rlist(i).node), None, None)
      } else if (rloc contains "Remote") {
        (None, None, Some(rlist(i).node))
      } else if (rloc contains "Local") {
        (None, Some(rlist(i).node), None)
      } else {
        (None, None, None)
      }
    }
  }

  // connect Mem channels up to mem
  mem.zipWithIndex.foreach { case (m, coreid) =>
    val read = readers(coreid)
    val write = writers(coreid)
    val arb = LazyModule(new TLXbar)
    read.zipWithIndex.foreach { case (r, i) =>
      if (readLoc(i) contains "Mem") {
        arb.node := readSplit(i)(coreid)._1.get
      }
    }
    write.zipWithIndex.foreach { case (w, i) =>
      if (writeLoc(i) contains "Mem") {
        arb.node := writeSplit(i)(coreid)._1.get
      }
    }
    m := TLBuffer() := arb.node
  }

  // connect producers+consumers to local buffer
  // localMem is a Seq of identity nodes correspnoding to a single write channel
  // this attaches to an xbar which splits into 8 per-core buf per channel
  // then a second xbar connects a single producer and single consumer

  // first when the buffer is at the producer
  val localWrite = writeLoc.zipWithIndex.map { case (wloc, i) =>
    if (wloc contains "Local") {
      val corenode = TLIdentityNode()
      val corearb = LazyModule(new TLXbar)
      (0 until nCores by bufStride).map { case coreid =>
        val loc = TLIdentityNode()
        val arb = LazyModule(new TLXbar)
        (0 until bufStride).foreach { case offset =>
          arb.node := TLBuffer() := writeSplit(i)(coreid + offset)._2.get
        }
        arb.node := TLBuffer() := loc
        producerBuffers(i).get(coreid / bufStride) := TLFragmenter(32, 128) := TLBuffer() := arb.node
        loc
      }.foreach { case loc_node =>
        loc_node := corearb.node
      }
      corearb.node := corenode
      Some(corenode)
    } else {
      None
    }
  }

  // then when the buffer is at the consumer
  val localRead = readLoc.zipWithIndex.map { case (rloc, i) =>
    if (rloc contains "Local") {
      val corenode = TLIdentityNode()
      val corearb = LazyModule(new TLXbar)
      (0 until nCores by bufStride).map { case coreid =>
        val loc = TLIdentityNode()
        val arb = LazyModule(new TLXbar)
        (0 until bufStride).foreach { case offset =>
          arb.node := TLBuffer() := readSplit(i)(coreid + offset)._2.get
        }
        arb.node := TLBuffer() := loc
        consumerBuffers(i).get(coreid / bufStride) := TLFragmenter(32, 128) := TLBuffer() := arb.node
        loc
      }.foreach { case loc_node =>
        loc_node := corearb.node
      }
      corearb.node := corenode
      Some(corenode)
    } else {
      None
    }
  }

  // create external ports for reading from remote types
  // will be connected to the localWrite of another syste
  // in Accelerator.scala
  val remoteRead = readLoc.zipWithIndex.map { case (rloc, i) =>
    if (rloc contains "Remote") {
      val corenode = TLIdentityNode()
      val corearb = LazyModule(new TLXbar)
      readSplit(i).foreach { case rnode =>
        corearb.node := rnode._3.get
      }
      corenode := corearb.node
      Some(corenode)
    } else {
      None
    }
  }
  val remoteWrite = writeLoc.zipWithIndex.map { case (wloc, i) =>
    if (wloc contains "Remote") {
      val corenode = TLIdentityNode()
      val corearb = LazyModule(new TLXbar)
      writers.zipWithIndex.foreach { case (wlist, coreid) =>
        corearb.node := writeSplit(i)(coreid)._3.get
      }
      corenode := corearb.node
      Some(corenode)
    } else {
      None
    }
  }

  def module: ModularSystemImp[ModularSystem]
}

abstract class ModularSystemImp[+L <: ModularSystem](val outer: L) extends LazyModuleImp(outer) {
  def io: ModularSystemIO

  def cores: Seq[ModularCore]

  //def respArbiter: RRArbiter[]

  lazy val cmd = Queue(io.cmd)

  lazy val funct = cmd.bits.inst.funct(2, 0)
  val funct_start = 0.U
  val funct_addr = 1.U

  lazy val coreSelect = cmd.bits.inst.rs1

  // val addressBits = (outer.mem ++ outer.producerBuffers.filter(_.isDefined).map(_.get).flatten.map(_.node) ++
  //   outer.remoteMem.filter(_.isDefined).map(_.get)).map { case m =>
  val addressBits = (outer.mem
    ++ outer.localWrite.filter(_.isDefined).map(_.get)
    ++ outer.remoteWrite.filter(_.isDefined).map(_.get)
    ++ outer.localRead.filter(_.isDefined).map(_.get)
    ++ outer.remoteRead.filter(_.isDefined).map(_.get)
    ).map { case m =>
    val edge = m.edges.in(0)
    log2Up(edge.manager.maxAddress)
  }.max

  val readLoc = if (outer.modularInterface.readLocations.isEmpty) {
    Seq.fill(outer.modularInterface.readElementSizes.length) {
      "Mem"
    }
  } else {
    outer.modularInterface.readLocations.get
  }
  val writeLoc = if (outer.modularInterface.writeLocations.isEmpty) {
    Seq.fill(outer.modularInterface.writeElementSizes.length) {
      "Mem"
    }
  } else {
    outer.modularInterface.writeLocations.get
  }

  lazy val numReadChannels = readLoc.length
  lazy val numWriteChannels = writeLoc.length
  lazy val numChannels = numReadChannels + numWriteChannels
  lazy val addrFile = Module(new SettingsFile(numChannels * cores.length, addressBits))

  lazy val resp = Wire(Decoupled(new RoCCResponse)) //Queue(io.resp)

  //TODO: figure out hwo to deal with type param for arbiter
  //val arbiter = Module(new RRArbiter(new DKAggregatorCoreResponse(addressBits), nCores))
  //arbiter.io.in <> cores.map(_.io.resp)
  //resp.valid := arbiter.io.out.valid
  //resp.bits.rd := arbiter.io.out.bits.rd
  //resp.bits.data := Cat(arbiter.io.out.bits.id, arbiter.io.out.bits.len.asTypeOf(UInt(32.W)))
  //arbiter.io.out.ready := resp.ready

  def coreRWReady(c: Int): Bool = {
    RegNext(outer.readers(c).map(_.module.io.req.ready).reduce(_ && _) &&
      outer.writers(c).map(_.module.io.req.ready).reduce(_ && _))
  }

  //TODO: Have to assign length manually for now
  lazy val readLengths = outer.readers.map(_.map(_.module.io.req.bits.len))
  lazy val writeLengths = outer.writers.map(_.map(_.module.io.req.bits.len))


  lazy val coreReady = cores.zipWithIndex.map { case (core, i) =>
    (core.io.req.ready && coreRWReady(i)) || (coreSelect =/= i.U)
  }.reduce(_ && _)

  // connect cores to mem
  def setupCode() {
    io.resp <> Queue(resp)

    if (outer.systemParams.doubleBuf) {
      outer.consumerBuffersModules.foreach { case bufs =>
        if (bufs.isDefined) {
          bufs.get.zipWithIndex.foreach { case (buf, i) =>
            buf.asInstanceOf[DoubleTLRAM].module.io.swap.valid := false.B
            when(cores(i).io.req.fire()) {
              buf.asInstanceOf[DoubleTLRAM].module.io.swap.valid := true.B
            }
          }
        }
      }
    }

    cores.zipWithIndex.foreach { case (core, i) =>
      // TO-DO: delay until all readers/writers are ready? use DecoupledHelper
      val coreStart = cmd.fire() && funct === funct_start && coreSelect === i.U //&& coreRWReady(i)
      core.io.req.valid := coreStart
      core.io.req.bits.id := coreSelect
      core.io.req.bits.rd := cmd.bits.inst.rd

      //rest are up to individual system
    }

    cmd.ready := funct =/= funct_start || coreReady

    io.busy := cores.map(_.io.busy).any

    addrFile.io.write.en := cmd.bits.inst.funct(2, 0) === funct_addr && cmd.fire()
    addrFile.io.write.addr := cmd.bits.rs1 + (numChannels.U * coreSelect)
    addrFile.io.write.data := cmd.bits.rs2

    // connect readChannels and writeChannels appropriately
    cores.zipWithIndex.foreach { case (core, i) =>
      val cmdFireLatch = RegNext(cmd.fire())
      val cmdBitsLatch = RegNext(cmd.bits)
      val functLatch = cmdBitsLatch.inst.funct(2, 0)
      val coreSelectLatch = cmdBitsLatch.inst.rs1

      core.readChannels.zipWithIndex.foreach { case (c, j) =>
        val r = outer.readers(i)(j)
        //        c.req <> r.module.io.req
        r.module.io.req.valid := cmdFireLatch && functLatch === funct_start && coreSelectLatch === i.U
        r.module.io.req.bits.addr := addrFile.io.values(i * numChannels + j)

        c <> SFQueue(outer.queueDepth, outer.modularInterface.readElementSizes(j))(r.module.io.channel)
      }

      core.writeChannels.zipWithIndex.foreach { case (c, j) =>
        val w = outer.writers(i)(j)
        //        c.req <> w.module.io.req
        w.module.io.req.valid := cmdFireLatch && functLatch === funct_start && coreSelectLatch === i.U
        w.module.io.req.bits.addr := addrFile.io.values(i * numChannels + numReadChannels + j)

        w.module.io.channel <> SFQueue(outer.queueDepth, outer.modularInterface.writeElementSizes(j))(c)
      }
    }

    // TO-DO: Generate value for r.module.io.req

  }

}
