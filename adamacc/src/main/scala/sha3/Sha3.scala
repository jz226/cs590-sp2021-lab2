package adamacc.sha3

import chisel3._
import chisel3.util._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.tile._
import freechips.rocketchip.coreplex.CacheBlockBytes
import freechips.rocketchip.util._
import freechips.rocketchip.config.{Parameters, Field}
import adamacc._
import adamacc.common._
import adamacc.common.Util._

/**
 * Sample configuration: 
 *   nMemXacts = 1, 
 *   readElementSizes = Seq(8), 
 *   writeElementSizes = Seq(8), 
 *   readLocations = Some(Seq("Mem")),
 *   writeLocations = Some(Seq("Mem")),
 *   inputBytes = 8,
 *   outputBytes = 8
 */

case class Sha3CoreParams(
  nMemXacts: Int, // not for production release
  readElementSizes: Seq[Int], // size in bytes
  writeElementSizes: Seq[Int], // size in bytes
  readLocations: Option[Seq[String]] = None,
  writeLocations: Option[Seq[String]] = None,
  inputBytes: Int,
  outputBytes: Int
) extends ModularCoreParams

case class Sha3Config(
  coreParams : Sha3CoreParams,
  opcode : Int,
  nCores: Int)
 extends ModularSystemParams {
  override def buildSystem(implicit p: Parameters): ModularSystem = new Sha3System(this)
}

case object Sha3Key extends Field[Option[Sha3Config]]

class Sha3CoreRequest(modularInterface : Sha3CoreParams)(implicit p: Parameters) extends ModularCoreBundle()(p) {
  val readLen = UInt(8.W)
  val writeLen = UInt(8.W)
  override def cloneType = new Sha3CoreRequest(modularInterface).asInstanceOf[this.type]
}

class Sha3CoreResponse()(implicit p: Parameters) extends ModularCoreBundle()(p){
  val error = UInt(32.W)
  override def cloneType = new Sha3CoreResponse().asInstanceOf[this.type]
}

class Sha3CoreIO(modularInterface : Sha3CoreParams)(implicit p: Parameters) extends ModularCoreIO()(p) {
  val req = Flipped(Decoupled(new Sha3CoreRequest(modularInterface)(p)))
  val resp = Decoupled(new Sha3CoreResponse())
  override def cloneType = new Sha3CoreIO(modularInterface).asInstanceOf[this.type]
}

class Sha3Core(addressBits : Int, modularInterface : Sha3CoreParams)(implicit p: Parameters) extends ModularCore(addressBits, modularInterface)(p) {
  val io = IO(new Sha3CoreIO(modularInterface)(p))

  val inputBytes = modularInterface.inputBytes
  val outputBytes = modularInterface.outputBytes

  val subcore = Module(new Sha3SubCore(inputBytes, outputBytes))
  subcore.io.input <> readChannels(0)
  writeChannels(0) <> subcore.io.output
  subcore.io.req <> io.req
  io.resp <> subcore.io.resp
  io.busy := subcore.io.busy
}

class Sha3System(systemParams : Sha3Config)(implicit p: Parameters) extends ModularSystem(systemParams)(p) {

  lazy val module = new ModularSystemImp(this) {
    val io = IO(new ModularSystemIO)
    val cores = Seq.fill(nCores) { Module(new Sha3Core(addressBits, systemParams.coreParams)(p)) }
    setupCode()

    // LocalEnum(3) means three instructions. 
    // 1. start signal (defined in ModularSystemImp)
    // 2. set address (defined in ModularSystemImp)
    // 3. set read/write length (defined here)
    val funct_setLen :: Nil = LocalEnum(3) 
    val readLen = Reg(Vec(nCores, UInt(8.W)))
    val writeLen = Reg(Vec(nCores, UInt(8.W)))

    // We only care about core 0
    when (cmd.fire()) {
      switch (funct) {
        is (funct_setLen) {
          readLen(0) := cmd.bits.rs1
          writeLen(0) := cmd.bits.rs2
        }
      }
    }

    cores.zipWithIndex.foreach { case(core, i) =>
      core.io.req.bits.readLen := readLen(i)
      core.io.req.bits.writeLen := writeLen(i)
      // notice readLengths(i) is a Seq, which contains multiple channels
      readLengths(i).foreach(_ := readLen(i)) // number of read elements
      writeLengths(i).foreach(_ := writeLen(i)) // number of write elements
    }

    // We only care about core 0
    val core_resp = cores(0).io.resp
    core_resp.ready := resp.ready
    resp.valid := core_resp.valid
    resp.bits.rd := core_resp.bits.rd
    // notice this data corresponds to the get_id_retval() in C test
    resp.bits.data := Cat(core_resp.bits.id, core_resp.bits.error) 
  }
}
