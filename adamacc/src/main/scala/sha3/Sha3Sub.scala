package adamacc.sha3

import chisel3._
import chisel3.util._
import freechips.rocketchip.config.{Field, Parameters}
import freechips.rocketchip.tile.RoCCResponse
import freechips.rocketchip.util._


class Sha3SubCoreRequest() extends Bundle {
  val id = UInt(5.W) // max 32 cores
  val rd = UInt(5.W) // 5 bits allocated for rd

  val readLen = UInt(8.W)
  val writeLen = UInt(8.W)
}

class Sha3SubCoreResponse() extends Bundle  {
  val id = UInt(5.W) // max 32 cores
  val rd = UInt(5.W) // 5 bits allocated for rd
  val error = UInt(32.W)
}

class Sha3SubCoreIO(inputBytes : Int, outputBytes : Int) extends Bundle {
  val req = Flipped(Decoupled(new Sha3SubCoreRequest))
  val resp = Decoupled(new Sha3SubCoreResponse)
  val busy = Output(Bool())
  val input = Flipped(new tempIO(inputBytes * 8))
  val output = new tempIO(outputBytes * 8)
  override def cloneType = new Sha3SubCoreIO(inputBytes, outputBytes).asInstanceOf[this.type]
}

class tempIO(maxBits: Int) extends Bundle {
  val data = Decoupled(UInt(maxBits.W))
  val stop = Input(Bool())
  val finished = Output(Bool())
  override def cloneType = new tempIO(maxBits).asInstanceOf[this.type]
}

class Sha3SubCore(inputBytes : Int, outputBytes : Int) extends Module {
  val io = IO(new Sha3SubCoreIO(inputBytes, outputBytes))

  val reqCache = Reg(new Sha3SubCoreRequest())
  val respCache = Reg(new Sha3SubCoreResponse())

  val state_test = RegInit(VecInit(Seq.fill(6)(0.U(30.W))))
  val s_idle :: s_read:: s_calc :: s_write :: s_finish :: s_error :: Nil = Enum(6)
  val state = RegInit(init = s_idle)
  val readLen = Reg(UInt(8.W))
  val writeLen = Reg(UInt(8.W))
  val readIdx = Reg(UInt(8.W))
  val writeIdx = Reg(UInt(8.W))

  io.req.ready := false.B
  io.input.stop := false.B
  io.input.data.ready := false.B
  // io.output
  io.output.data.valid := false.B
  io.output.data.bits := 0.U 
  io.output.finished := false.B
  // io.resp
  io.resp.bits.id := respCache.id
  io.resp.bits.rd := respCache.rd
  io.resp.bits.error := respCache.error
  io.resp.valid := false.B
  // io.busy
  io.busy := state =/= s_idle

  // io.req
  when (state === s_idle) {
    io.req.ready := true.B
    when (io.req.fire()) {
      reqCache := io.req.bits
      readLen := io.req.bits.readLen
      writeLen := io.req.bits.writeLen
      respCache.id := io.req.bits.id
      respCache.rd := io.req.bits.rd
      respCache.error := 0.U
      readIdx := 0.U
      state := s_read
    }
  }

  // read and write 64 bits per cycle
  // 17 read cycles
  // 4 write cycles
  val sha3InReg = RegInit(VecInit(Seq.fill(17)(0.U(64.W))))
  // set to 1 for testing
  // you should use this to check if you C test can read the FPGA output
  val sha3OutReg = RegInit(VecInit(Seq.fill(4)(1.U(64.W))))

  // Import your sha3 accelerator as a module
  // val sha3Accel = Module(new Sha3Accel(64))

  // connect data signals
  // sha3Accel.io.message.bits := sha3InReg
  // sha3OutReg := sha3Accel.io.hash.bits

  // connect control signals
  // ...

  when (state === s_read) {
    io.input.data.ready := true.B
    when (io.input.data.fire()) {
      sha3InReg(readIdx) := (io.input.data.bits).asTypeOf(sha3InReg(0))
      readIdx := readIdx + 1.U
    }
    when(readIdx === readLen - 1.U) {
      state := s_calc
    }
  }

  when (state === s_calc) {
    // ???
    // when( ??? ) {
      state := s_write
      writeIdx := 0.U
    // }
  }

  when (state === s_write) {
    io.output.data.valid := true.B
    io.output.data.bits := sha3OutReg(writeIdx)
    when(io.output.data.fire()) {
      writeIdx := writeIdx + 1.U
    }
    when (writeIdx === writeLen - 1.U) {
      state := s_finish
    }
  }

  when(state === s_finish) {
    io.input.stop := true.B
    io.output.finished := true.B
    io.resp.valid := true.B
    when (io.resp.fire()) { 
      state := s_idle 
    }
  }
}
